# Kodi Media Center language file
# Addon Name: VBox TV Gateway PVR Client
# Addon id: pvr.vbox
# Addon Provider: Sam Stenvall
msgid ""
msgstr ""
"Project-Id-Version: KODI Main\n"
"Report-Msgid-Bugs-To: https://github.com/xbmc/xbmc/issues/\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Kodi Translation Team\n"
"Language-Team: Hungarian (Hungary) (http://www.transifex.com/projects/p/kodi-main/language/hu_HU/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hu_HU\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgctxt "#30000"
msgid "Local network"
msgstr "Helyi hálózat"

msgctxt "#30001"
msgid "Hostname or IP address"
msgstr "Kiszolgálónév vagy IP cím"

msgctxt "#30002"
msgid "HTTP port"
msgstr "HTTP Port"

msgctxt "#30003"
msgid "UPnP port"
msgstr "UPnP port"

msgctxt "#30004"
msgid "Connection timeout"
msgstr "Kapcsolat időtúllépés"

msgctxt "#30005"
msgid "HTTPS port (set to 0 to use plain HTTP)"
msgstr "HTTPS port (0 az alap HTTP)"

msgctxt "#30006"
msgid "Connection"
msgstr "Kapcsolat"

msgctxt "#30007"
msgid "Internet"
msgstr "Internet"

msgctxt "#30020"
msgid "Channel & EPG"
msgstr "Csatorna és EPG"

msgctxt "#30021"
msgid "Channels"
msgstr "Csatornák"

msgctxt "#30022"
msgid "EPG"
msgstr "EPG"

msgctxt "#30023"
msgid "Channel numbers set by"
msgstr "Csatornaszámozás ez alapján:"

msgctxt "#30024"
msgid "LCN (Logical Channel Number) from backend"
msgstr "LCN (Logikai csatornaszám) a háttérrendszertől"

msgctxt "#30025"
msgid "Channel index in backend"
msgstr "Csatornaszámozás a háttérrendszerben"

msgctxt "#30026"
msgid "Reminder time (minutes before program starts)"
msgstr "Emlékeztetési idő (percekben a műsor kezdete előtt)"

msgctxt "#30027"
msgid "Skip initial EPG load"
msgstr "Kezdeti EPG-betöltés átugrása"

msgctxt "#30040"
msgid "Timeshift"
msgstr "Csúsztatott felvétel"

msgctxt "#30041"
msgid "Enable timeshifting"
msgstr "Csúsztatott élőkép engedélyezése"

msgctxt "#30042"
msgid "Timeshift buffer path"
msgstr "Csúsztatott élőkép tároló útvonala"

msgctxt "#30106"
msgid "VBox device rescan of EPG (will take a while)"
msgstr "VBox eszköz EPG újrapásztázás (eltart egy darabig)"

msgctxt "#30107"
msgid "Sync EPG"
msgstr "EPG szincronizálása"

msgctxt "#30110"
msgid "Remind me"
msgstr "Emlékeztető"

msgctxt "#30111"
msgid "Manual reminder"
msgstr "Kézi emlékeztető"

msgctxt "#30112"
msgid "Cancel reminder (if exists)"
msgstr "Emlékeztető törlése (ha létezik)"

msgctxt "#30113"
msgid "Cancel all the channel's reminders"
msgstr "A csatorna összes emlékeztetőjének törlése"

msgctxt "#30600"
msgid "Contains settings for connecting to the VBox device from both your local network and from the internet. A local connection will be attempted first and if unsuccessful the internet settings will be used."
msgstr "A VBox készülékhez való csatlakozáshoz szükséges beállításokat tartalmazza mind a helyi hálózatról, mind az internetről. Először megkísérli a helyi kapcsolatot, és ha sikertelen, akkor az internetbeállításokat fogja használni."

msgctxt "#30601"
msgid "The IP address or hostname of your VBox when accessed from the local network."
msgstr "A VBox IP-címe vagy gazdaneve, amikor a helyi hálózatról érik el."

msgctxt "#30602"
msgid "The port used to connect to your VBox when accessed from the local network. Default value is `80`."
msgstr "A VBox csatlakozásához használt port, amikor a helyi hálózatról érik el. Az alapértelmezett érték: 80."

msgctxt "#30603"
msgid "The port used to connect to your VBox if using HTTPS when accessed from the local network. The default `0` means this is disabled and HTTP will be used instead."
msgstr "A VBox  csatlakozásához használt port ha HTTPS-t használ, amikor a helyi hálózatról érkezik. Az alapértelmezett \"0\" azt jelenti, hogy ez le van tiltva, és helyette a HTTP lesz használatban."

msgctxt "#30604"
msgid "The port used to connect to your VBox via UPnP when accessed from the local network. Default value is `55555`."
msgstr "Az a port, amely UPnP-n keresztül csatlakozik a VBox-hoz amikor a helyi hálózatról érik el. Az alapértelmezett érték: 55555."

msgctxt "#30605"
msgid "The value used (in seconds) to denote when a connection attempt has failed when accessed from the local network. Default value is `3`."
msgstr "Az az érték (másodpercben), amely jelzi ha helyi hálózatról a csatlakozási kísérlet sikertelen volt. Az alapértelmezett érték \"3\"."

msgctxt "#30606"
msgid "The IP address or hostname of your VBox when accessed from the internet."
msgstr "A VBox IP-címe vagy gazdaneve, ha az internetről fér hozzá."

msgctxt "#30607"
msgid "The port used to connect to your VBox when accessed from the internet."
msgstr "A VBox satlakozásához használt port, amikor az internetről van hozzáférés."

msgctxt "#30608"
msgid "The port used to connect to your VBox if using HTTPS when accessed from the internet. The default `0` means this is disabled and HTTP will be used instead."
msgstr "A VBox csatlakozásához használt port ha HTTPS-t használ amikor az internetről fér hozzá. Az alapértelmezett \"0\" azt jelenti, hogy ez le van tiltva és helyette a HTTP lesz használatban."

msgctxt "#30609"
msgid "The port used to connect to your VBox via UPnP when accessed from the internet. Default value is `55555`."
msgstr "Az a port amellyel az UPnP-n keresztül csatlakozik a VBox-hoz, amikor az internetről hozzáfér. Az alapértelmezett érték: 55555."

msgctxt "#30610"
msgid "The value used (in seconds) to denote when a connection attempt has failed when accessed from the internet. Default value is `10`."
msgstr "Az az érték (másodpercben) amely azt jelzi ha egy csatlakozási kísérlet sikertelen volt, ha az internetről érkezett. Az alapértelmezett érték: 10."

msgctxt "#30620"
msgid "Settings related to Channels & EPG."
msgstr "A csatornákkal és az EPG-vel kapcsolatos beállítások."

msgctxt "#30621"
msgid "Channel numbers can be set via either of the following two options: [LCN (Logical Channel Number) from backend] The channel numbers as set on the backend; [Channel index in backend] Starting from 1 number the channels as per the order they appear on the backend."
msgstr "A csatornaszámok a következő két lehetőség valamelyikével állíthatók be: [LCN (Logical Channel Number) a háttérből] A csatornaszámok a háttérben állnak; [Csatornaindex a háttérben] Az 1-től kezdve számozza a csatornákat a háttérrendszerben megjelenő sorrend szerint."

msgctxt "#30622"
msgid "The amount of time in minutes prior to a programme start that a reminder should pop up."
msgstr "Emlékeztető megjelenítése, a program futtatásához szükséges idő percben."

msgctxt "#30623"
msgid "Ignore the initial EPG load. Enabled by default to prevent crash issues on LibreElec/CoreElec."
msgstr "Figyelmen kívül hagyja a kezdeti EPG-terhelést. Alapértelmezésben engedélyezve, hogy megakadályozzák a LibreElec / CoreElec összeomlását."

msgctxt "#30640"
msgid "Settings related to the timeshift."
msgstr "A csúsztatott felvételhez (timeshift) kapcsolódó beállítások."

msgctxt "#30641"
msgid "If enabled allows pause, rewind and fast-forward of live TV."
msgstr "Ha az engedélyezve van,akkor lehetővé teszi a szüneteltetést, a visszatekerést és az előrehaladást élő adás esetén."

msgctxt "#30642"
msgid "The path where the timeshift buffer files should be stored when timeshifting is enabled. Make sure you have a reasonable amount of disk space available since the buffer will grow indefinitely until you stop watching or switch channels."
msgstr "Az az útvonal ahova az időeltolásos pufferfájlokat tárolni kell amikor az időeltolás (timeshift) engedélyezve van. Győződjön meg róla, hogy van elegendő szabad lemezterület mivel a puffer határozatlan ideig növekszik amíg meg nem állítja a nézést vagy csatornaváltást."
