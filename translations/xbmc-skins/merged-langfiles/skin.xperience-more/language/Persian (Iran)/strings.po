# Kodi Media Center language file
# Addon Name: Xperience More
# Addon id: skin.xperience-more
# Addon Provider: Nessus
msgid ""
msgstr ""
"Project-Id-Version: XBMC Skins\n"
"Report-Msgid-Bugs-To: alanwww1@xbmc.org\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Kodi Translation Team\n"
"Language-Team: Persian (Iran) (http://www.transifex.com/projects/p/xbmc-skins/language/fa_IR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fa_IR\n"
"Plural-Forms: nplurals=1; plural=0;\n"

msgctxt "#31001"
msgid "Live TV"
msgstr "تلویزیون زنده"

msgctxt "#31016"
msgid "Music Files"
msgstr "فایل های موسیقی"

msgctxt "#31020"
msgid "Recently Added"
msgstr "اخیراً افزوده شده"

msgctxt "#31113"
msgid "Folder:"
msgstr "پوشه:"

msgctxt "#31127"
msgid "Single Image"
msgstr "تصویر تکی"

msgctxt "#31128"
msgid "Multi Image"
msgstr "چند تصویره"

msgctxt "#31213"
msgid "Aired"
msgstr "پخش شده"

msgctxt "#31225"
msgid "Now Playing"
msgstr "در حال پخش"

msgctxt "#31241"
msgid "Version"
msgstr "ویرایش"

msgctxt "#31243"
msgid "Lyrics Source"
msgstr "منبع اشعار"

msgctxt "#31321"
msgid "Poster"
msgstr "پوستر"

msgctxt "#31351"
msgid "Last Updated"
msgstr "آخرین بروزرسانی"

msgctxt "#31352"
msgid "Data Provider"
msgstr "ارائه دهنده خدمات داده"

msgctxt "#31356"
msgid "at"
msgstr "از"

msgctxt "#31357"
msgid "Hide Fanart"
msgstr "مخفی کردن Fanart"

msgctxt "#31358"
msgid "Set Fanart Path"
msgstr "انتخاب مسیر Fanart"

msgctxt "#31359"
msgid "Forecast"
msgstr "پیش بینی"

msgctxt "#31362"
msgid "Hourly Forecast"
msgstr "پیش بینی یک ساعته"

msgctxt "#31363"
msgid "Weekend Forecast"
msgstr "پیش بینی هفتگی"

msgctxt "#31365"
msgid "Chance of Precipitation"
msgstr "احتمال بارندگی"

msgctxt "#31366"
msgid "Fetching forecast info..."
msgstr "دریافت اطلاعات پیش بینی..."

msgctxt "#31500"
msgid "Page"
msgstr "صفحه"

msgctxt "#31501"
msgid "of"
msgstr "از"

msgctxt "#31502"
msgid "Items"
msgstr "موضوعات"

msgctxt "#32101"
msgid "Add Group"
msgstr "افزودن گروه جدید"

msgctxt "#32102"
msgid "Rename Group"
msgstr "تغییر نام گروه"

msgctxt "#32103"
msgid "Delete Group"
msgstr "حذف گروه"

msgctxt "#32104"
msgid "Now"
msgstr "اکنون"

msgctxt "#32105"
msgid "Next"
msgstr "بعدی"
